// XPTItemCargos.hpp
// Used for defining ammo box and vehicle item cargos
// XPTVehicleLoadouts can pull definitions from here in order to supply vehicles
// Supports sub-class randomization, if a definition has multiple sub-classes, the script will automatically select one of them to apply to the vehicle/box.

/* 
	Randomization support
	
	--- Sub-class randomization --- 
	
	Any itemCargos class can define items through a sub-class instead of in the main class.
	In this case, the script will pick one of the sub-classes at random to apply to the object.
	The names of the sub-classes do not matter.
	
	The following example will select one of "box1" or "box2" when the "MyAmmoBox" class is called.
	The names of the subclasses don't matter, and they can be named whatever you want them to be.
	
	class itemCargos
	{
		class MyAmmoBox
		{
			class box1
			{
				items[] = {};
			};
			class box2
			{
				items[] = {};
			};
		};
	};
	
	
	--- Per-item randomization ---
	
	Any item can have the quantity changed from a fixed value, to a randomized value.
	By creating an array of three numbers instead of a single one, the itemCargo script will randomly select a quantity using the provided numbers.
	
	Example:
	
	class MyAmmoBox
	{
		items[] = {"FirstAidKit", {1,3,5}}; // Spawns a minimum of 1, a maximum of 5, and an average of 3
	};

*/
class itemCargos
{
	class example
	{
		// Array containing sub-arrays of items to add
		// Sub-arrays must include an item classname, and a quantity
		// The following would add 5 first aid kits to the inventory of the object
		items[] = {
			{"FirstAidKit", 5},
			{"FirstAidKit", {1,3,5}}
		};
		// Array of items that will only be added when ACE basic medical is being used
		itemsBasicMed[] = {
			{"FirstAidKit", 5},
			{"FirstAidKit", {1,3,5}}
		};
		// Array of items that will only be added when ACE advanced medical is being used
		itemsAdvMed[] = {
			{"FirstAidKit", 5},
			{"FirstAidKit", {1,3,5}}
		};
	};
	
	class empty
	{
		items[] = {};
		itemsBasicMed[] = {};
		itemsAdvMed[] = {};
	};
	
	class vehicleBasic
	{
		items[] = {
			{"11Rnd_45ACP_Mag", 12},
			{"1Rnd_HE_Grenade_shell", 10},
			{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36", 16},
			{"10Rnd_338_Mag", 10},
			{"CUP_100Rnd_TE4_LRT4_Red_Tracer_762x51_Belt_M", 10},
			{"HandGrenade", 10},
			{"SmokeShellGreen", 4},
			{"SmokeShellRed", 4},
			{"SmokeShellPurple", 4},
			{"SmokeShell", 4},
			{"1Rnd_SmokeGreen_Grenade_shell", 4},
			{"1Rnd_SmokeRed_Grenade_shell", 4},
			{"1Rnd_SmokePurple_Grenade_shell", 4},
			{"1Rnd_Smoke_Grenade_shell", 4}
		};
		itemsBasicMed[] = {
			{"ACE_fieldDressing", 30},
			{"ACE_bloodIV", 5},
			{"ACE_epinephrine", 10},
			{"ACE_adenosine",10},
			{"ACE_morphine", 5}
		};
		itemsAdvMed[] = {};
	};
	
	class vehicleHeavy
	{
		items[] = {
			{"ToolKit", 3},
			{"SmokeShellGreen", 4},
			{"SmokeShellRed", 4},
			{"SmokeShellPurple", 4},
			{"SmokeShell", 4}
		};
		itemsBasicMed[] = {
			{"ACE_fieldDressing", 30},
			{"ACE_bloodIV", 5},
			{"ACE_epinephrine", 10},
			{"ACE_adenosine",10},
			{"ACE_morphine", 5}
		};
		itemsAdvMed[] = {};
	};
	
	class vehicleRepair
	{
		items[] = {
			{"ToolKit", 3}
		};
		itemsBasicMed[] = {};
		itemsAdvMed[] = {};
	};
	
	class ammoboxLarge
	{
		items[] = {
			{"ACE_EntrenchingTool", 5},
			{"11Rnd_45ACP_Mag", 12},
			{"1Rnd_HE_Grenade_shell", 20},
			{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36", 100},
			{"10Rnd_338_Mag", 20},
			{"CUP_100Rnd_TE4_LRT4_Red_Tracer_762x51_Belt_M", 25},
			{"HandGrenade", 20},
			{"SmokeShellGreen", 20},
			{"SmokeShellRed", 20},
			{"SmokeShellPurple", 20},
			{"SmokeShell", 20},
			{"1Rnd_SmokeGreen_Grenade_shell", 10},
			{"1Rnd_SmokeRed_Grenade_shell", 10},
			{"1Rnd_SmokePurple_Grenade_shell", 10},
			{"1Rnd_Smoke_Grenade_shell", 10},
			{"Titan_AT", 10}
		};
		itemsBasicMed[] = {
			{"ACE_fieldDressing", 100},
			{"ACE_bloodIV", 10},
			{"ACE_epinephrine", 45},
			{"ACE_adenosine",30},
			{"ACE_morphine", 15}
		};
		itemsAdvMed[] = {};
	};
};