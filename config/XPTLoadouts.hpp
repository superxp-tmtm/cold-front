// XPTloadouts.hpp
// Used for defining advanced respawn loadouts for players
// Default behaviour is to check if the player unit has a special loadout defined. Otherwise, it will check to see if the classname matches a loadout
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class loadouts
{	
	// Empty loadout with comments removed. Use this for your loadouts
	class base
	{
		displayName = "Base Loadout";
		
		primaryWeapon[] = {"", "", "", "", {}, {}, ""};
		secondaryWeapon[] = {"", "", "", "", {}, {}, ""};
		handgunWeapon[] = {"", "", "", "", {}, {}, ""};
		binocular = "";
		
		uniformClass = "";
		headgearClass = "";
		facewearClass = "";
		vestClass = "";
		backpackClass = "";
		
		linkedItems[] = {"ItemMap", "ItemGPS", "ItemRadio", "ItemCompass", "ItemWatch", "NVGoggles"};
		
		uniformItems[] = {};
		vestItems[] = {};
		backpackItems[] = {};
		
		basicMedUniform[] = {};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		
		advMedUniform[] = {};
		advMedVest[] = {};
		advMedBackpack[] = {};
	};
	
	class nato_base: base
	{
		displayName = "NATO Base Loadout";

		primaryWeapon[] = {};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"hgun_Pistol_heavy_01_green_F","","acc_flashlight_pistol","",{"11Rnd_45ACP_Mag",11},{},""};
		binocular = "Binocular";

		uniformClass = "U_B_CTRG_3";
		headgearClass = "H_HelmetSpecB";
		vestClass = "V_PlateCarrierL_CTRG";

		linkedItems[] = {"ItemMap","ItemMicroDAGR","TFAR_anprc152","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"ItemcTabHCam",1},{"11Rnd_45ACP_Mag",1,11},{"SmokeShell",2,1}};
		vestItems[] = {};

		basicMedUniform[] = {{"ACE_fieldDressing",10},{"ACE_epinephrine",3},{"ACE_adenosine",2},{"ACE_morphine",1}};
		basicMedVest[] = {};

	};
	
	class B_Officer_F: nato_base
	{
		displayName = "NATO Commander"; // Currently unused, basically just a human-readable name for the loadout

		primaryWeapon[] = {"CUP_arifle_G36C","","cup_acc_flashlight","CUP_optic_CompM2_low",{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",30},{},""};
		binocular = "Laserdesignator";

		uniformClass = "U_B_CTRG_2";
		headgearClass = "H_Beret_02";
		facewearClass = "G_Aviator";
		vestClass = "V_PlateCarrierL_CTRG";
		backpackClass = "B_RadioBag_01_wdl_F";

		linkedItems[] = {"ItemMap","ItemcTab","TFAR_anprc152","ItemCompass","ItemWatch",""};

		vestItems[] = {{"ACE_EntrenchingTool",1},{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",7,30},{"HandGrenade",2,1},{"SmokeShell",2,1},{"SmokeShellRed",2,1},{"SmokeShellGreen",2,1}};
	};
	
	class B_Medic_F: nato_base
	{
		displayName = "NATO Medic"; // Currently unused, basically just a human-readable name for the loadout

		primaryWeapon[] = {"CUP_arifle_G36KA3_afg","","cup_acc_flashlight","optic_MRCO",{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",30},{},""};

		headgearClass = "H_HelmetSpecB_sand";
		facewearClass = "G_Tactical_Clear";
		vestClass = "V_PlateCarrierH_CTRG";
		backpackClass = "B_Carryall_oli";

		vestItems[] = {{"ACE_EntrenchingTool",1},{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",9,30},{"SmokeShell",4,1},{"SmokeShellGreen",3,1},{"SmokeShellRed",3,1}};

		basicMedBackpack[] = {{"ACE_fieldDressing",100},{"ACE_bloodIV",15},{"ACE_epinephrine",30},{"ACE_adenosine",20},{"ACE_morphine",10}};	
		advMedBackpack[] = {};
	};
	
	class cmd_medic: B_Medic_F
	{
		displayName = "NATO Command Medic"
		
		binocular = "Rangefinder";
		
		backpackClass = "FRXA_tf_rt1523g_big_Ranger_Green";
		
		linkedItems[] = {"ItemMap","ItemcTab","TFAR_anprc152","ItemCompass","ItemWatch",""};
		
		basicMedBackpack[] = {{"ACE_fieldDressing",65},{"ACE_bloodIV",5},{"ACE_epinephrine",25},{"ACE_adenosine",10},{"ACE_morphine",10}};
		advMedBackpack[] = {};
	};
	
	class B_Crew_F: nato_base
	{
		displayName = "NATO Crewman";

		primaryWeapon[] = {"CUP_arifle_G36C","","cup_acc_flashlight","CUP_optic_CompM2_low",{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",30},{},""};

		uniformClass = "U_B_CTRG_1";
		headgearClass = "H_HelmetCrew_B";
		facewearClass = "G_Tactical_Clear";
		vestClass = "V_PlateCarrierL_CTRG";
		backpackClass = "B_AssaultPack_rgr";

		vestItems[] = {{"ACE_EntrenchingTool",1},{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",7,30},{"SmokeShell",4,1},{"SmokeShellGreen",2,1},{"SmokeShellRed",2,1}};
		backpackItems[] = {{"ToolKit",1}};
	};
	
	class crew_lead: B_Crew_F
	{
		displayName = "NATO Crew Leader";
		
		binocular = "Rangefinder";
		
		backpackClass = "FRXA_tf_rt1523g_big_Ranger_Green";
		
		linkedItems[] = {"ItemMap","ItemAndroid","TFAR_anprc152","ItemCompass","ItemWatch",""};
		
		vestItems[] = {{"ACE_EntrenchingTool",1},{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",7,30},{"SmokeShell",4,1},{"SmokeShellGreen",2,1},{"SmokeShellRed",2,1}};
		backpackItems[] = {{"ToolKit", 1}};
	};
	
	class arty_op: crew_lead
	{
		displayName = "Artillery Operator";
		backpackClass = "FRXA_tf_rt1523g_big_Ranger_Green";
		vestItems[] = {{"ACE_artilleryTable",1},{"ACE_EntrenchingTool",1},{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",9,30},{"SmokeShell",4,1},{"SmokeShellGreen",2,1},{"SmokeShellRed",2,1}};
	};
	
	class B_Soldier_SL_F: nato_base
	{
		displayName = "NATO Squad Leader";

		primaryWeapon[] = {"CUP_arifle_G36K_RIS_AG36","","cup_acc_flashlight","optic_MRCO",{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",30},{"1Rnd_HE_Grenade_shell",1},""};
		binocular = "Laserdesignator";

		uniformClass = "U_B_CTRG_3";
		headgearClass = "H_HelmetSpecB_blk";
		facewearClass = "G_Tactical_Clear";
		vestClass = "V_PlateCarrierH_CTRG";
		backpackClass = "B_RadioBag_01_wdl_F";

		linkedItems[] = {"ItemMap","ItemAndroid","TFAR_anprc152","ItemCompass","ItemWatch",""};

		vestItems[] = {{"ACE_EntrenchingTool",1},{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",9,30},{"SmokeShell",2,1},{"SmokeShellGreen",2,1},{"SmokeShellRed",2,1},{"HandGrenade",2,1}};
		backpackItems[] = {{"1Rnd_HE_Grenade_shell",11,1},{"1Rnd_Smoke_Grenade_shell",3,1},{"1Rnd_SmokeRed_Grenade_shell",3,1},{"1Rnd_SmokeGreen_Grenade_shell",3,1}};
	};
	
	class B_Soldier_TL_F: nato_base
	{
		displayName = "NATO Team Leader";

		primaryWeapon[] = {"CUP_arifle_G36K_RIS_AG36","","cup_acc_flashlight","optic_MRCO",{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",30},{"1Rnd_HE_Grenade_shell",1},""};
		binocular = "Rangefinder";

		facewearClass = "G_Tactical_Clear";
		vestClass = "V_PlateCarrierH_CTRG";
		backpackClass = "B_AssaultPack_rgr";

		vestItems[] = {{"ACE_EntrenchingTool",1},{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",9,30},{"SmokeShell",2,1},{"SmokeShellGreen",2,1},{"SmokeShellRed",2,1},{"HandGrenade",2,1}};
		backpackItems[] = {{"1Rnd_HE_Grenade_shell",11,1},{"1Rnd_Smoke_Grenade_shell",3,1},{"1Rnd_SmokeRed_Grenade_shell",3,1},{"1Rnd_SmokeGreen_Grenade_shell",3,1}};
	};
	
	class B_Soldier_M_F: nato_base
	{
		displayName = "NATO Marksman";

		primaryWeapon[] = {"srifle_DMR_02_camo_F","","cup_acc_flashlight","optic_AMS",{"10Rnd_338_Mag",10},{},"bipod_01_F_blk"};
		binocular = "Rangefinder";

		facewearClass = "G_Tactical_Clear";

		vestItems[] = {{"ACE_EntrenchingTool",1},{"ACE_RangeCard",1},{"10Rnd_338_Mag",7,10},{"SmokeShell",2,1},{"SmokeShellGreen",2,1},{"SmokeShellRed",2,1}};
	};
	
	class B_Soldier_AR_F: nato_base
	{
		displayName = "NATO Autorifleman";

		primaryWeapon[] = {"CUP_lmg_Mk48_nohg_wdl","","cup_acc_flashlight_wdl","optic_tws_mg",{"CUP_100Rnd_TE4_LRT4_Red_Tracer_762x51_Belt_M",100},{},""};

		facewearClass = "G_Tactical_Clear";
		backpackClass = "B_AssaultPack_rgr";
		
		vestItems[] = {{"ACE_EntrenchingTool",1},{"optic_MRCO",1},{"CUP_100Rnd_TE4_LRT4_Red_Tracer_762x51_Belt_M",2,100},{"SmokeShell",2,1}};
		backpackItems[] = {{"CUP_100Rnd_TE4_LRT4_Red_Tracer_762x51_Belt_M",3,100}};
	};
	
	class B_Soldier_AT_F: nato_base
	{
		displayName = "NATO AT Specialist";

		primaryWeapon[] = {"CUP_arifle_G36KA3_afg","","cup_acc_flashlight","optic_MRCO",{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",30},{},""};
		secondaryWeapon[] = {"launch_B_Titan_short_F","","","",{"Titan_AT",1},{},""};

		backpackClass = "B_AssaultPack_cbr";

		vestItems[] = {{"ACE_EntrenchingTool",1},{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",9,30},{"SmokeShell",2,1},{"SmokeShellGreen",2,1},{"SmokeShellRed",2,1},{"HandGrenade",2,1}};
		backpackItems[] = {{"Titan_AT",1,1}};
	};
	
	class B_Soldier_AAT_F: nato_base
	{
		displayName = "NATO Asst. AT Specialist";

		primaryWeapon[] = {"CUP_arifle_G36KA3_afg","","cup_acc_flashlight","optic_MRCO",{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",30},{},""};

		backpackClass = "B_Carryall_cbr";

		vestItems[] = {{"ACE_EntrenchingTool",1},{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",9,30},{"SmokeShell",2,1},{"SmokeShellGreen",2,1},{"SmokeShellRed",2,1},{"HandGrenade",2,1}};
		backpackItems[] = {{"Titan_AT",3,1}};
	};
	
	class B_Soldier_A_F: nato_base
	{
		displayName = "NATO Ammo Bearer";
		binocular = "Rangefinder";

		primaryWeapon[] = {"CUP_arifle_G36KA3_afg","","cup_acc_flashlight","optic_MRCO",{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",30},{},""};

		backpackClass = "B_Carryall_khk";

		vestItems[] = {{"ACE_EntrenchingTool",1},{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",9,30},{"SmokeShell",2,1},{"SmokeShellGreen",2,1},{"SmokeShellRed",2,1},{"HandGrenade",2,1}};
		backpackItems[] = {{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",15,30},{"CUP_100Rnd_TE4_LRT4_Red_Tracer_762x51_Belt_M",4,100}};
	};
	
	class B_Engineer_F: nato_base
	{
		displayName = "NATO Logistics Engineer";

		primaryWeapon[] = {"CUP_arifle_G36KA3_afg","","cup_acc_flashlight","optic_MRCO",{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",30},{},""};

		backpackClass = "B_AssaultPack_rgr";

		vestItems[] = {{"ACE_EntrenchingTool",1},{"CUP_30Rnd_TE1_Red_Tracer_556x45_G36",9,30},{"SmokeShell",2,1},{"SmokeShellGreen",2,1},{"SmokeShellRed",2,1},{"HandGrenade",2,1}};
		backpackItems[] = {{"ToolKit",1}};
	};
	
	class logi_lead: B_Engineer_F
	{
		displayName = "NATO Logistics Lead";
		backpackClass = "FRXA_tf_rt1523g_big_CTRG";
		linkedItems[] = {"ItemMap","ItemAndroid","TFAR_anprc152","ItemCompass","ItemWatch",""};
	};
};