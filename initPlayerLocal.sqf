// initPlayerLocal.sqf
// Executes on a client machine when they load the mission, regardless of if it's at mission start or JIP.
// _this = [player:Object, didJIP:Boolean]
params ["_player", "_jip"];

// Call the template initPlayerLocal function
_this call XPT_fnc_initPlayerLocal; // DO NOT CHANGE THIS LINE

// Add any mission specific code after this point
[] execVM "scripts\briefing.sqf";
[] execVM "scripts\zeusMenu.sqf";

// Define the air raid siren action
private _airRaidActionMain = ["sxp_airRaid","Air Raid Siren","",{true},{player isKindOf "VirtualCurator_F"}] call ace_interact_menu_fnc_createAction;
private _airRaidActionConfirm = ["sxp_airRaid_confirm","Confirm Air Raid","",{[] remoteExec ["SXP_fnc_airRaid",2]},{player isKindOf "VirtualCurator_F"}] call ace_interact_menu_fnc_createAction;

// Add actions to zeus
[["ACE_ZeusActions"], _airRaidActionMain] call ace_interact_menu_fnc_addActionToZeus;
[["ACE_ZeusActions", "sxp_airRaid"], _airRaidActionConfirm] call ace_interact_menu_fnc_addActionToZeus;

// Add actions for remote controlled units
["Man", 1, ["ACE_SelfActions"], _airRaidActionMain, true] call ace_interact_menu_fnc_addActionToClass;
["Man", 1, ["ACE_SelfActions", "sxp_airRaid"], _airRaidActionConfirm, true] call ace_interact_menu_fnc_addActionToClass;