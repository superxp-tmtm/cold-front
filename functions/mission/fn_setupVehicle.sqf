// SXP_fnc_setupVehicle
// Handles setting up vehicles at mission start, and upon respawn

// Only run on the server
if (!isServer) exitWith {};

// Define variables
_this params [
	["_newVeh", nil, [objNull]],
	["_oldVeh", nil, [objNull]]
];

// If newVeh is nil, exit the script
if (isNil "_newVeh") exitWith {};

// Start our switch block, check the vehicle classname to determine what needs to be done for setup
switch (toLower (typeOf _newVeh)) do {
	// M4 Scorcher
	case (toLower "B_MBT_01_arty_F"): {
		// Lock the driver's seat
		_newVeh lockDriver true;
		
		// Remove all fuel from the vehicle
		_newVeh setFuel 0;
		
		// Enable datalink
		_newVeh setVehicleReportRemoteTargets true;
		_newVeh setVehicleReceiveRemoteTargets true;
		_newVeh setVehicleReportOwnPosition true;

		_newVeh addEventHandler ["GetIn", {
			params ["_vehicle", "_role", "_unit", "_turret"];
			[_vehicle,["6Rnd_155mm_Mo_mine",[0]]] remoteExec ["removeMagazineTurret", 0];
			[_vehicle,["6Rnd_155mm_Mo_AT_mine",[0]]] remoteExec ["removeMagazineTurret", 0];
		}];
	};
	// IFV-6a Cheetah
	case (toLower "B_APC_Tracked_01_AA_F"): {
		// Lock the driver's seat
		_newVeh lockDriver true;
		
		// Remove all fuel from the vehicle
		_newVeh setFuel 0;
		
		// Enable datalink
		_newVeh setVehicleReportRemoteTargets true;
		_newVeh setVehicleReceiveRemoteTargets true;
		_newVeh setVehicleReportOwnPosition true;
	};
	// Basic Vehicles
	case (toLower "CUP_B_MTVR_Ammo_USMC");
	case (toLower "CUP_B_Mastiff_HMG_GB_W");
	case (toLower "CUP_B_LR_Special_M2_GB_W");
	case (toLower "CUP_B_FV432_Bulldog_GB_W"): {
		[_newVeh, "vehicleBasic"] call XPT_fnc_loadItemCargo;
	};
	
	// Heavy Vehicles
	case (toLower "B_T_AFV_Wheeled_01_up_cannon_F"): {
		[_newVeh, "vehicleHeavy"] call XPT_fnc_loadItemCargo;
		
		// Enable datalink
		_newVeh setVehicleReportRemoteTargets true;
		_newVeh setVehicleReceiveRemoteTargets true;
		_newVeh setVehicleReportOwnPosition true;
		
		[
			_newVeh,
			["Green",1], 
			["showCamonetHull",0,"showCamonetTurret",0,"showSLATHull",1]
		] call BIS_fnc_initVehicle;
	};
	
	case (toLower "CUP_B_M2A3Bradley_USA_W");
	case (toLower "B_MBT_01_TUSK_F"): {
		[_newVeh, "vehicleHeavy"] call XPT_fnc_loadItemCargo;
		
		// Enable datalink
		_newVeh setVehicleReportRemoteTargets true;
		_newVeh setVehicleReceiveRemoteTargets true;
		_newVeh setVehicleReportOwnPosition true;
	};
	
	// Repair truck
	case (toLower "CUP_B_MTVR_Repair_USMC"): {
		[_newVeh, "vehicleRepair"] call XPT_fnc_loadItemCargo;
	};
	
	// Fuel truck
	case (toLower "CUP_B_MTVR_Refuel_USMC"): {
		[_newVeh, "empty"] call XPT_fnc_loadItemCargo;
	};
	
	// Avengers
	case (toLower "CUP_B_nM1097_AVENGER_USA_WDL"): {
		[_newVeh, "empty"] call XPT_fnc_loadItemCargo;
		
		// Enable datalink
		_newVeh setVehicleReportRemoteTargets true;
		_newVeh setVehicleReceiveRemoteTargets true;
		_newVeh setVehicleReportOwnPosition true;
	};
};