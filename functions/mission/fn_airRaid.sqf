// SXP_fnc_airRaid
// Plays an air-raid siren effect from multiple locations

if (!isServer) exitWith {};

_soundPath = [(str missionConfigFile), 0, -15] call BIS_fnc_trimString;
_siren = _soundPath + "media\sounds\air_raid_siren.ogg";

_speakers = [
	siren_1,
	siren_2,
	siren_3,
	siren_4,
	siren_5,
	siren_6,
	siren_7,
	siren_8
] call BIS_fnc_arrayShuffle;

{
	playSound3D [_siren, _x, false, getPosASL _x, 5, 1, 250];
	sleep 1;
} forEach _speakers;